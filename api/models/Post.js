/**
* Post.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		location: { type: 'string', defaultsTo: "" },
		latitude: { type: 'string', defaultsTo: "" },
		longitude: { type: 'string', defaultsTo: "" },
		description: { type: 'string', defaultsTo: "" },
		imageUrl: { type: 'string', defaultsTo:"" },
		owner: {
			model: 'user'
		},
		favorites: {
			collection: 'favorite',
			via: 'post'
		}
	}
};

