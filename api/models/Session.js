/**
* Session.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		sessionToken: { type: 'string' , unique: true },
		owner: {
			model: 'user'
		}
	},

	createToken: function() {
		var rand = function() {
		    return Math.random().toString(36).substr(2); // remove `0.`
		};

		var token = function() {
		    return rand() + rand(); // to make it longer
		};

		return token();
	}


};

