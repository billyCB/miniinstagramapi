/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	attributes: {
		username: { type: 'string' , unique: true },
		password: { type: 'string'},
		avatar: { type: 'string', defaultsTo: "" },
		about: { type: 'string', defaultsTo: "" },
		numberFollowers: { type: 'integer', defaultsTo: 0 },
		numberFollowing: { type: 'integer', defaultsTo: 0 },
		numberPosts: { type: 'integer', defaultsTo: 0 },
		sessions: {
			collection: 'session',
			via: 'owner'
		},
		posts: {
			collection: 'post',
			via: 'owner'
		},
		followers:{
			collection: 'followers',
			via: 'owner'
		},
		favorites:{
			collection: 'favorite',
			via: 'user'
		}
	}
};

