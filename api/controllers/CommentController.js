/**
 * CommentController
 *
 * @description :: Server-side logic for managing comments
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	createComment: function (req, res) {
		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				User.findOne({ id: session.owner }).exec(function  findOneCB(err, userQuery){
					Comment.create({ user: userQuery,
						post: req.body.postId, comment: req.body.comment, aboutUser: userQuery }).exec(function createCB(err,created){
					  	if (err) {
					  		return res.json({ message: "Error when trying to create post" });
					  	}else{
					  		return res.json(created);
					  	}
					});
				});				
			}
		});
	},
	postAllComment: function (req, res){
		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session) {
			if (typeof session === 'undefined') {
				return res.json({ err: 'no exist token' });
			}else{
				Comment.find({ post: req.query.postId }).exec(function findOneCB(err, comments){
					return res.json( comments );
				});
			}
		});
	}
};

