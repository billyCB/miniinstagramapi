/**
 * PostController
 *
 * @description :: Server-side logic for managing posts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	createPost: function (req, res) {

		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				Post.create({ owner: session.owner,
					description: req.body.postDescription,
					location : req.body.postLocation,
					latitude : req.body.postLatitude,
					longitude : req.body.postLongitude,
					imageUrl : req.body.imageUrl }).exec(function createCB(err,created){
				  	if (err) {
				  		message = { message: "Error when trying to create post" };
				  	}else{
				  		message = created;
				  	}
				  	User.findOne({id: created.owner }).exec(function findOneCB(err, user){
						if (typeof user !== 'undefined') {
							message.owner = user;
						}
					  	return res.json(message);
					});
				});
			}
		});
	},
	getFeedPost: function (req, res) {
		
		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				Post.find().exec(function findOneCB(err, posts){
					if (err) {
						return res.json({ err: "Error when trying to get all post" });
					}else{
						for (var index in posts) {
							var user;
							User.findOne({ id: posts[index].owner }).exec(function  findOneCB(err, userQuery) {
								user = userQuery;
								Favorite.find({ where: { post: posts[index].id, user: session.owner }}).exec(function findOneCB(err, favorite){
									if (favorite.length != 0 ){
										posts[index].favorite = true;
									}else{
										posts[index].favorite = false;
									}
									posts[index].owner = user;
									if ( posts.length = (index + 1) ) {
										return res.json( posts );
									}
								});
							});						
						}																
					}
				});		
			}
		});

		
			
	}




};

