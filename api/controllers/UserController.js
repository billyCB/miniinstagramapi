/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	getAllPostByUser: function (req, res) {
		if (req.headers["min-instagram-session-token"] == "" || req.headers["min-instagram-session-token"] == null) {
			return res.json({ err: "no authenticate"});
		}

		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				User.findOne({id: req.query.user_id }).exec(function findOneCB(err, user){
					if (typeof user === 'undefined') {
						return res.json({ err: "Bad username" } );
					}else{
						Post.find({owner: user.id }).exec(function findOneCB(err, posts){
							if (typeof posts !== 'undefined') {
								for (var index in posts) {
									posts[index].owner = user
								};
								if (posts.length == 0) {
									return res.json({ err: "no posts"});
								}
								return res.json(posts);
							}
						  	return res.json({ err: "no posts"});
						});
					}
					

				});
				
			}
		});
	},
	getCurrentFollowers: function(req, res){
		if (req.headers["min-instagram-session-token"] == "" || req.headers["min-instagram-session-token"] == null) {
			return res.json({ err: "no authenticate"});
		}
		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				Followers.find({ owner: session.owner }).exec(function findOneCB(err, followers){
					if (typeof followers === 'undefined') {
						return res.json({ err: "problem in server" } );
					}else{
						return res.json({ followers: followers});		
					}				
				});
				
			}
		});
	},
	getCurrentFollowings: function(req, res){
		if (req.headers["min-instagram-session-token"] == "" || req.headers["min-instagram-session-token"] == null) {
			return res.json({ err: "no authenticate"});
		}
		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				Followers.find({ follower: session.owner }).exec(function findOneCB(err, followings){
					if (typeof followings === 'undefined') {
						return res.json({ err: "problem in server" } );
					}else{
						return res.json({ followings: followings});		
					}				
				});
				
			}
		});
	}

};

