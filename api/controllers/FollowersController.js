/**
 * FollowersController
 *
 * @description :: Server-side logic for managing followers
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	follow: function (req, res) {
		if (req.headers["min-instagram-session-token"] == "" || req.headers["min-instagram-session-token"] == null) {
			return res.json({ err: "no authenticate"});
		}
		console.log(req);

		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				Followers.create({ owner: req.body.owner, follower: req.body.follower }).exec(function createCB(err,created){
				  	if (err) {
				  		return res.json({ message: "Error when trying to follow user" });
				  	}else{
				  		User.findOne({id: req.body.owner }).exec(function findOneCB(err, user){
				  			console.log("found user ==> " + user  + " user id ==> " + req.body.owner);
				  			user.numberFollowers++;
							user.save(function(err){
								if (err) {
									return res.json({ err: "Error when trying to update user" });
								}else {	
									return res.json({ message: "success follow" });
								}
							});
				  		});
				  	}
				});
				
			}
		});
	},

	unfollow: function (req, res) {
		if (req.headers["min-instagram-session-token"] == "" || req.headers["min-instagram-session-token"] == null) {
			return res.json({ err: "no authenticate"});
		}

		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				Followers.findOne({owner: req.body.owner, follower: req.body.follower }).exec(function findOneCB(err, follow){
					if (typeof follow === 'undefined') {
						return res.json({ err: "Bad request" } );
					}else{
						follow.destroy(function(err) {
							if (err) {
								return res.json({ message: "Error when trying to update user" });
							}else{
								User.findOne({id: req.body.owner }).exec(function findOneCB(err, user){
						  			user.numberFollowers--;
									user.save(function(err){
										if (err) {
											return res.json({ err: "Error when trying to update user" });
										}else {
											console.log("success update user");
											return res.json({ message: "success unfollow" });
										}
									});
						  		});
							}						
       						
			            });						
					}				
				});
				
			}
		});
	}
};

