/**
 * FavoriteController
 *
 * @description :: Server-side logic for managing favorites
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	favorite: function (req, res) {
		if (req.headers["min-instagram-session-token"] == "" || req.headers["min-instagram-session-token"] == null) {
			return res.json({ err: "no authenticate"});
		}

		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				Favorite.create({ post: req.body.post, user: session.owner }).exec(function createCB(err,created){
				  	if (err) {
				  		return res.json({ message: "Error when trying to favorited post" });
				  	}else{
				  		return res.json({ message: "success favorite" });
				  	}
				});
				
			}
		});
	},

	unfavorite: function (req, res) {
		if (req.headers["min-instagram-session-token"] == "" || req.headers["min-instagram-session-token"] == null) {
			return res.json({ err: "no authenticate"});
		}

		Session.findOne({sessionToken: req.headers["min-instagram-session-token"] }).exec(function findOneCB(err, session){
			if (typeof session === 'undefined') {
				return res.json({ err: "no exist token" } );
			}else{
				Favorite.findOne({post: req.body.post, user: session.owner }).exec(function findOneCB(err, favoriteRecord){
					if (typeof favoriteRecord === 'undefined') {
						return res.json({ err: "Bad request" } );
					}else{
						favoriteRecord.destroy(function(err) {
							if (err) {
								return res.json({ message: "Error when trying to unfavorite post" });
							}else{
								return res.json({ message: "success unfavorite" });
							}						
			            });						
					}				
				});
				
			}
		});
	}
};

