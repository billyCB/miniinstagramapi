/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	login: function (req, res) {
		var message;
		User.findOne({username: req.body.username }).exec(function findOneCB(err, user){
			if (typeof user === 'undefined') {
				return res.json({ err: "Bad username" } );
			}else{
				if (user.password == req.body.password ) {
					Session.create({ sessionToken: Session.createToken() , owner: user.id }).exec(function createCB(err,created){
					  	if (err) {
					  		message = { message: "Error when trying to create session" };
					  	}else{
					  		message = { session: created, user: user };
					  	}
					  	return res.json(message);
					});
				}else{
					return res.json({ err: "Bad Password" });
				}				
			}
		});
	}

};

