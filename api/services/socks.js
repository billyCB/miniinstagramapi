/**
 * We load SocketIO
 */
var http = require("http"),
    server = http.createServer().listen(8080);
 
var io = require('socket.io').listen(server), // Don't forget to "npm install socket.io" before including this
    fs = require('fs');

var socketsArray = [];
var roomsArray = [];

/**
 * Class User
 */
function UserSocket(id, username){
    this.id = id;
    this.username = username;
}

 
/**
 * Connection
 */
 
 io.sockets.on('connection', function (socket) {

	socket.roomchat = [];
	
	socket.on('new user', function(data){
		socket.user = new UserSocket(data.id, data.username); 
		socketsArray.push(socket);
		socket.emit("uss", null );
		sendAllUser(socket);
	});

	socket.on('new room', function(data){
		var partnerSocket = findSocketByUserId(data.userId);
		var roomCreated = "" + data.userId + socket.user.id + "";
		var params = { roomName: roomCreated, partnerSocket:  partnerSocket, currentSocket: socket };
		createRoom(params);
	});

	socket.on('disconnect', function () {
	  	socketsArray = _.filter(socketsArray, function(s){ return s.user != socket.user});
		socket.broadcast.emit('user left', {
			user: socket.user
		});
	});
});

/**
 * Create Room
 */

function createRoom(params){
	params.currentSocket.join(params.roomName);
	params.partnerSocket.join(params.roomName);
	
	params.currentSocket.roomchat.push(params.roomName);
	params.partnerSocket.roomchat.push(params.roomName);
	roomsArray.push(params);

	params.currentSocket.broadcast.to(params.roomName).on('chat message', function(msg){
		params.currentSocket.broadcast.to(params.roomName).emit('chat message', msg);
	});

	params.partnerSocket.broadcast.to(params.roomName).on('chat message', function(msg){
		params.partnerSocket.broadcast.to(params.roomName).emit('chat message', msg);
	});

	params.partnerSocket.emit('sucess new room', { partnerUser: params.partnerSocket.user, roomName: params.roomName } );
	params.currentSocket.emit('new chat room', { partnerUser: params.currentSocket.user, roomName: params.roomName } );
}

/*
* Emit sockets
*/

function sendAllUser(socket){
	var usersArray = _.map(socketsArray, function(obj){ return obj.user; });
	socket.emit("total users", { users : usersArray });
	socket.broadcast.emit("new user" , { user: socket.user });
}


/*
 *	Find socket by user id
 */

function findSocketByUserId(userId){
	var socketPartner= _.find(socketsArray, function(sock){ 
		return sock.user.id == userId; 
	});
	return socketPartner;
}


/**
 * Let's export everything
 */

 module.exports = {
 
     socket: io.sockets,
 
 }