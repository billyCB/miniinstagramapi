angular.module("ChatMiniinstagramApp", ["LocalStorageModule", "ngRoute"])
.config(function($routeProvider){
	$routeProvider
		.when("/",{
			controller: "LoginController",
			templateUrl: "js/views/login-template.html"
		})
		.when("/chat/:userId",{
			controller: "ChatController",
			templateUrl: "js/views/chat-template.html"
		})
		.when("/list-user",{
			controller: "ListUserController",
			templateUrl: "js/views/list-user-template.html"
		})
});