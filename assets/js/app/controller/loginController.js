angular.module("ChatMiniinstagramApp")
.controller("LoginController", function($window, $scope, localStorageService, $http) {

	/**
 	* register new user 
 	*/

	$scope.setUser = function(){
		var params = { id: $('#userId').val() , username: $('#userName').val() };
		$scope.$parent.user = params;
		console.log("setUser");
		$scope.socket.emit('new user', params);
	}

});