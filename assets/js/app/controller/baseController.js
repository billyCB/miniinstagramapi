angular.module("ChatMiniinstagramApp")
.controller("BaseController", function($window, $scope, localStorageService, $location, $http) {
	$scope.user;
	$scope.socket = io("http://localhost:8080/");
	$scope.users = [];
	$scope.chats = [];

	/**
	 * get user list
 	*/

	$scope.socket.on("new chat room", function(data){
		if ( !$scope.existChatByRoomName(data.roomName)) {
			$scope.chats.push(data);
			$location.path('/chat/'+ data.partnerUser.id);
			$scope.$apply();
		}
	});

	$scope.socket.on("sucess new room",function(data){
		console.log(data);
		$scope.chats.push(data);
	});

	$scope.socket.on("total users",function(data){
		$location.path('/list-user');
		$scope.users = data.users;
		$scope.$apply();
	});

	$scope.socket.on("new user",function(data){
		$scope.users.push(data.user);
		$scope.$apply();
	});

	$scope.socket.on("user left",function(data){
		$scope.users = _.filter($scope.users, function(s){  return s.id != data.user.id; });
		$scope.$apply();
	});

	/*
	 *	Find chat by room name
	 */

	$scope.existChatByRoomName = function(roomName){
		var chat =  _.find($scope.chats, function(chat){ return chat.roomName == roomName; });
		return chat;
	};

});