angular.module("ChatMiniinstagramApp")
.controller("ChatController", function($window, $scope, localStorageService, $http, $routeParams) {
	$scope.userId = $routeParams.userId;
	$scope.messages = [];

	/*
		Set new room 
	 */

	if ( !$scope.existChatByRoomName( ""+ $scope.$parent.user.id + $scope.userId ) ) {
		$scope.socket.emit("new room", { userId : $scope.userId });
	};

	/*
		Socket Listener
	 */
	
	$scope.socket.on('chat message', function(msg){
		$scope.messages.push(msg);
		$scope.$apply();
	});

	/*
		Actions
	 */
	
	$scope.sendMessage = function(){
  		var msg = { message: $('#messageInput').val() , user: $scope.$parent.user };
  		$scope.socket.emit('chat message', msg);
  		$('#messageInput').val('');
  		$scope.messages.push(msg);
	};

});